function show() {
    // Get the checkbox
    var checkBox = document.getElementById("myCheck");
    // Get the output text
    var showdate = document.getElementById("showdate");

    // If the checkbox is checked, display the output text
    if (checkBox.checked == true){
      showdate.style.display = "block";
    } else {
      showdate.style.display = "none";
    }
  }

  // source : https://www.w3schools.com/howto/howto_js_display_checkbox_text.asp