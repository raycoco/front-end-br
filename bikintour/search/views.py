from django.shortcuts import render
from .models import *

# Create your views here.


def search(request):
    boats = Boat.objects.all()
    context = {'boats': boats}
    return render(request, 'search/index.html', context)


def searchList(request):
    context = {}
    return render(request, 'search/searchList.html', context)


def cart(request):
    context = {}
    return render(request, 'search/ticketCollections.html', context)


def payment(request):
    context = {}
    return render(request, 'search/payment.html', context)


def boatList(request):
    context = {}
    return render(request, 'search/boatList.html', context)
