from django.urls import path
from . import views

urlpatterns = [
    path('', views.search, name="index"),
    path('searchList/', views.searchList, name="search"),
    path('cart/', views.cart, name="cart"),
    path('payment/', views.payment, name="payment"),
    path('boatList/', views.boatList, name="boat list"),
]
